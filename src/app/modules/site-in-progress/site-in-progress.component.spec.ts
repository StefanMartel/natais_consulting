import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteInProgressComponent } from './site-in-progress.component';

describe('SiteInProgressComponent', () => {
  let component: SiteInProgressComponent;
  let fixture: ComponentFixture<SiteInProgressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteInProgressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteInProgressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
