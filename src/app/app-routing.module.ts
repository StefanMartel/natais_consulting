import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiteInProgressComponent } from './modules/site-in-progress/site-in-progress.component';

const routes: Routes = [
  { path: '', redirectTo: 'siteInProgress', pathMatch: 'full'},
  { path: 'siteInProgress', component: SiteInProgressComponent},
  { path: '**', redirectTo: 'siteInProgress'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
